"""
Project Euler Problem 12

Find a Triangle number with over 500 different divisors.

Need a prime factorizer.
Need an arithmetic sum.

How it ties together:
    Need to generate a list of prime numbers.
    Take a triangle number, and split it into two equations:
        Divisors(Triangle) = D(Triangle / 2) * D(n + 1) if n is even
        Divisors(Triangle) = D(Triangle) * D((Triangle + 1) / 2) if n + 1 is even

    D() will find the number of divisors for the number.
    If Divisors(Triangle) > 500, then Triangle is our number!
"""

import unittest
import math
import functools
import operator
from ddt import ddt, data, file_data, unpack

def isPrime(number):
    if number < 2:
        return False

    if number > 1 and number < 4:
        return True

    if number % 2 == 0 or number % 3 == 0:
        return False

    if number < 9:
        return True

    limit = math.floor(math.sqrt(number))

    for num in range(5, limit + 1, 6):
        if number % num == 0:
            return False

        if (number) % (num + 2) == 0:
            return False

    return True

def findPrimesUpTo(number):
    prime_list = []
    if number < 2:
        return prime_list
    else:
        prime_list.append(2)
        for num in range(1, number + 1, 2):
            if isPrime(num):
                prime_list.append(num)

    return prime_list

def findNumberDivisorsFor(number, prime_list):
    exp_counter = dict()
    list_length = len(prime_list)
    count = 0
    if number == 1:
        return 1
    while number > 1 and count < list_length:
        prime = prime_list[count]
        if number % prime == 0:
            number = number // prime
            if prime not in exp_counter:
                exp_counter[prime] = 1
            else:
                exp_counter[prime] += 1
            count -= 1
        count += 1
    return functools.reduce(operator.mul, [value + 1 for value in exp_counter.values()])

def findTriangleNumberWithDivisors():
    prime_list = findPrimesUpTo(10000)
    divisors_count = 0
    number = 1
    while divisors_count < 500:
        divisors_count = 0
        triangle = number * (number + 1) // 2
        divisors_count = findNumberDivisorsFor(triangle, prime_list)
        number += 1
        print(number)

    return number - 1, triangle

@ddt
class TestTriangleDivisorFinder(unittest.TestCase):

    @data((28, 6), (12, 6), (45, 6))
    def test_NumberOfDivisorsForTriangleNum(self, value):
        input_val, expected = value
        actual = findNumberDivisorsFor(input_val)
        self.assertEqual(actual, expected)

    @data((1,[]), (2, [2]), (3, [2,3]), (4, [2,3]), (24, [2,3,5,7,11,13,17,19,23]))
    def test_PrimesUpTo(self, value):
        input_val, expected = value
        actual = findPrimesUpTo(input_val)
        self.assertEqual(actual, expected)

    @data(1, 4, 6, 8, 10, 12, 14, 15, 16, 18, 20, 21, 22, 24, 25, 26, 27, 28, 30, 45)
    def test_isPrimeFalse(self, value):
        result = isPrime(value)
        self.assertFalse(result)

    @data(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43)
    def test_isPrimeTrue(self, value):
        result = isPrime(value)
        self.assertTrue(result)

if __name__ == '__main__':
    # unittest.main()
    print(findTriangleNumberWithDivisors())
