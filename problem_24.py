"""
Find the millionth lexicographic permutation of the digits 0..9.

Direct Method for Solving:

    Generate all possible permutations of the digits 0..9.
    Sort this list of permutations.
    Return millionth element in list.
"""

import itertools
import unittest


def create_to_string_gen(integer_list):
    return (str(integer) for integer in integer_list)


def create_permutations_gen(list_of_char):
    return itertools.permutations(list_of_char)


def find_nth_permutation(unique_integers, nth):
    """
    Takes a container of unique integers and generates all possible
    permutations.
    Sorts all permutations in numerically ascending order.
    Creates a generator that converts the nth permutation into a tuple of
    strings.
    Returns the joined string resulting from joining the tuple.
    """
    string_gen = create_to_string_gen(unique_integers)
    permutation_gen = create_permutations_gen(string_gen)
    list_of_permutations = sorted(permutation_gen)
    return ''.join(list_of_permutations[nth - 1])


class TestNthPermutationFinder(unittest.TestCase):
    def test_millionthpermutation(self):
        nth = 1000000
        expected = '2783915460'
        actual = find_nth_permutation([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], nth)
        self.assertEqual(expected, actual)


if __name__ == '__main__':
    unittest.main()
