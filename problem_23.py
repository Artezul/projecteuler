"""
Find the sum of all positive integers that are below or equal to 28123.

Steps:
    Find all abundant numbers less than or equal to 28123
        Find all prime numbers less than or equal to the square root of 28123
        Determine whether or not a number is 'abundant'
            Find sum of divisors for each number

    Find all numbers that can't be derived from a sum of abundant numbers.
"""
import math

# Find all prime numbers less than N
def is_prime(number):
    """
    This function was taken from problem_10.py of my ProjectEuler set.
    """
    # One isn't a prime number
    if number == 1:
        return False

    # Numbers below 4 are 2 and 3, but prime.
    if number < 4:
        return True

    # All even numbers aren't prime.
    if number % 2 == 0:
        return False

    # The number isn't 1, 2, 3, 4, 6, 8 from the previous checks.
    # The remaining numbers are 5, 7.
    if number < 9:
        return True

    # Multiples of 3 aren't prime either.
    if number % 3 == 0:
        return False
    else:
        # Factors above limit DNE, and the number is prime.
        limit = math.floor(math.sqrt(number))

        # Pretty cool
        # With each iteration, we make a leap of 6 numbers, and the resulting
        # numbers are typically prime numbers.
        factor = 5
        while factor <= limit:
            if number % factor == 0:
                return False
            if number % (factor + 2) == 0:
                return False

            factor += 6

    return True

def find_all_primes(limit):
    list_of_primes = []
    for number in range(1, limit):
        if is_prime(number):
            list_of_primes.append(number)
    return list_of_primes

def prime_factorization(number, list_of_primes):
    import collections
    limit = math.floor(math.sqrt(number))
    count_of_prime_factors = collections.defaultdict(int)

    index = 0
    while number != 1:
        prime = list_of_primes[index]
        while number % prime == 0:
            number //= prime
            count_of_prime_factors[prime] += 1
        index += 1

    return count_of_prime_factors

def sum_of_divisors(number, list_of_primes):
    count_of_prime_factors = prime_factorization(number, list_of_primes)
    total = 1
    for key, value in count_of_prime_factors.items():
        # Equation finds sum of divisors plus original number
        total *= (((key**(value + 1)) - 1) / (key - 1))

    # We only want sum of divisors not equal to original number
    return total - number

def is_abundant_number(number, list_of_primes):
    return True if sum_of_divisors(number, list_of_primes) > number else False

def find_all_abundant_numbers(limit):
    import math
    list_of_primes = find_all_primes(limit + 1)
    list_of_abundant = []
    for number in range(1, limit + 1):
        if is_abundant_number(number, list_of_primes):
            list_of_abundant.append(number)

    return list_of_abundant

def sum_of_non_abundant_numbers(limit, list_of_abundant_numbers):
    index = 0
    abundant_sums = set()
    for abundant in list_of_abundant_numbers:
        # Slice the list each time, reducing the number of values to be
        # combined after each loop.
        trunc_abundants = list_of_abundant_numbers[index:]
        for trunc_abundant in trunc_abundants:
            abundant_sum = abundant + trunc_abundant
            if abundant_sum <= limit:
                abundant_sums.add(abundant_sum)
    integers = set(range(1, limit + 1))
    return sum(integers - abundant_sums)

print(sum_of_non_abundant_numbers(28123, find_all_abundant_numbers(28123)))
