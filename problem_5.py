"""
[2, 2, 2, 2, 3, 3, 5, 7, 11, 13, 17, 19]

Funnily enough, I solved this problem with math.  The above is a list
of all factors that will be in this number.

The requirement is that the number be divisible by all numbers from 1
to 20.

Starting from 2 to 20, as long as the number can be constructed from
the factors available, then we'll naturally have the lowest possible
number.


Now to solve this programically, and with the above understanding of
the problem:

Count from 2 to n.
For each number, find the prime factorization of that number, and
if it isn't already in the list, add the difference to the list.
The end result will be similar to the list above.
"""
import math
import unittest

def prime_factorization(number):
    limit = math.floor(math.sqrt(number))
    factor = 2
    factor_list = []
    while limit > 1 and number >= limit:
        if number % factor == 0:
            number = number // factor
            factor_list.append(factor)
            factor -= 1
        factor += 1

    if number != 1:
        factor_list.append(number)

    return factor_list

def FindSmallestNumberDivisibleFromOneTo(n):
    """
    Program extracts the prime factors that make up a number, and
    proceeds to count the number of occurrences of each factor.
    The greatest number of occurrences for each factor is kept
    until the desired range of divisible numbers is reached.

    Afterwards, a sorted list containing all of these prime
    factors is returned.

    Future optimization considerations:
        Use the Counter dictionary subclass
        Keep a separate count of each occurrence of a prime factor
            Afterwards, find the max of each, and keep it.
    """
    master = []
    for number in range(2, n + 1):
        factor_list = prime_factorization(number)
        count_dict = dict()
        for factor in factor_list:
            if factor not in count_dict:
                count_dict[factor] = 1
            else:
                count_dict[factor] += 1

        for key, value in count_dict.items():
            while master.count(key) < value:
                master.append(key)

    master.sort()

    return master

class TestFindSmallestNumberDivisibleByRange(unittest.TestCase):

    # @unittest.skip("Checking Prime Factorization First")
    def test_SmallestNumberDivisible(self):
        expected = [2, 2, 2, 2, 3, 3, 5, 7, 11, 13, 17, 19]
        actual = FindSmallestNumberDivisibleFromOneTo(20)
        self.assertEqual(expected, actual)

    def test_PrimeFactorization16_2222(self):
        expected = [2,2,2,2]
        actual = prime_factorization(16)
        self.assertEqual(expected, actual)

if __name__ == '__main__':
    unittest.main()
