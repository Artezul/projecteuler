"""
Project Euler Problem 8 - August 12th 2015

73167176531330624919225119674426574742355349194934
96983520312774506326239578318016984801869478851843
85861560789112949495459501737958331952853208805511
12540698747158523863050715693290963295227443043557
66896648950445244523161731856403098711121722383113
62229893423380308135336276614282806444486645238749
30358907296290491560440772390713810515859307960866
70172427121883998797908792274921901699720888093776
65727333001053367881220235421809751254540594752243
52584907711670556013604839586446706324415722155397
53697817977846174064955149290862569321978468622482
83972241375657056057490261407972968652414535100474
82166370484403199890008895243450658541227588666881
16427171479924442928230863465674813919123162824586
17866458359124566529476545682848912883142607690042
24219022671055626321111109370544217506941658960408
07198403850962455444362981230987879927244284909188
84580156166097919133875499200524063689912560717606
05886116467109405077541002256983155200055935729725
71636269561882670428252483600823257530420752963450

Find the thirteen adjacent digits in the 1000-digit number that have the
greatest product. What is the value of this product?

The block of digits is actually supposed to be a single giant string of numbers.

Not sure if it's cheating, but I'll just reduce it into a single line string,
and process it from there (as opposed to reading it from file, as is, and do
some massaging.)

There are a few ways I could go about this:

    Straight-forward:           Iterate through all the numbers.
    Maybe-Clever-tedious:       Delete blocks of numbers that are adjacent to
                                    the number 0.
                                Iterate, but skip blocks of thirteen whenever
                                    a 0 is encountered.
                                    Caveat:  If there's a zero inside the block
                                        to be skipped, skip up to that zero
                                        instead.
"""
import unittest
from functools import reduce
import operator

def mul(list_of_numbers):
    return reduce(operator.mul, list_of_numbers, 1)

def CharReader(block_of_text):
    """
    CharReader assumes the text as been formatted into a single string without
    any newlines before, after, or within said string.
    """
    for character in block_of_text:
        yield character

def FileReader(file_name):
    """
    Takes a file name, opens it in the current directory, and reads it line
    by line, appending it into a single string.  When the loop has finished,
    it will return a single string containing the entire contents of the file.
    """
    txt_blk = ""
    FILE = open(file_name, 'r')
    for line in FILE:
        txt_blk += line
    FILE.close()
    txt_blk = txt_blk.replace("\n", "")

    return txt_blk

def LargestProductInSeries(txt_blk):
    """
    Get the string of numbers
    Populate list with 13 numbers
    Repeat Until End
        Check if product is greater than current
            If it is, replace product
        Pop first element in list
        Append new element to end
    Return Largest Product
    """
    slots = []
    gen = CharReader(txt_blk)
    for num in range(13):
        slots.append(int(next(gen)))

    product = mul(slots)

    for char in gen:
        slots.pop(0)
        slots.append(int(char))
        product = max(mul(slots), product)

    return product

def main():
    txt = FileReader("problem_8_data.txt")
    return LargestProductInSeries(txt)


class TestLargestProductInSeries(unittest.TestCase):
    def setUp(self):
        self.dum_txt = FileReader("problem_8_testdata.txt")
        self.txt_blk = FileReader("problem_8_data.txt")

    def test_LineReaderabcd_a_b_c_d(self):
        gen = CharReader(self.dum_txt)
        lst = list(gen)
        string = "".join(lst)
        self.assertEqual(string, "1234")

    def test_Mul_2_11_4__88(self):
        result = mul([2,11,4])
        self.assertEqual(result, 88)

    def test_Mul_0_11_4__0(self):
        result = mul([0,11,4])
        self.assertEqual(result, 0)

if __name__ == '__main__':
    # unittest.main()
    print(main())
