"""
Find the unit fraction that contains the longest recurring cycle in its
decimal representation.
"""
