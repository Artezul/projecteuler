# Project Euler Problem 18 - Find the maximum sum of a sequence of adjacent
# numbers starting from the top of the triangle to the bottom.

# [[3],
# [7, 4],
# [2, 4, 6],
# [8, 5, 9, 3]]

# [75]
# [95, 64]
# [17, 47, 82]
# [18, 35, 87, 10]
# [20, 04, 82, 47, 65]
# [19, 01, 23, 75, 03, 34]
# [88, 02, 77, 73, 07, 63, 67]
# [99, 65, 04, 28, 06, 16, 70, 92]
# [41, 41, 26, 56, 83, 40, 80, 70, 33]
# [41, 48, 72, 33, 47, 32, 37, 16, 94, 29]
# [53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14]
# [70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57]
# [91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48]
# [63, 66, 04, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31]
# [04, 62, 98, 27, 23, 9, 70, 98, 73, 93, 38, 53, 60, 04, 23]

# Find the path with the greatest sum
# Loop through each element in the last list
# For each element and it's right neighbor, find the max of the two
# Add the larger element to the next to last list
# Pop list when finished
# Return element when list size is 1

require 'rspec'

# Find the path that leads to the largest sum
class MaxPathFinder
  attr_accessor :list

  def initialize(list)
    @list = list
  end

  def max
    # -2 because 0 based count, and skipping first array element.
    count = size - 2
    (0..count).each { step }
    @list.first.first
  end

  def step
    fail StopIteration if size == 1
    prev = @list.pop
    curr = @list.last
    len = curr.size - 1

    (0..len).each do |num|
      curr[num] += [prev[num], prev[num + 1]].max
    end
  end

  def size
    @list.size
  end
end

describe MaxPathFinder do
  before :each do
    @single_list = [[5]]
    @multi_list = [[5], [3, 9]]
    @single_max_path = MaxPathFinder.new(Array.new(@single_list))
    @multi_max_path = MaxPathFinder.new(Array.new(@multi_list))
  end

  describe '#max' do
    context 'given the list is size 1' do
      it 'should give the root value' do
        expect(@single_max_path.max).to eq(@single_list.first.first)
      end
    end

    context 'given a list greater than 1' do
      it 'should reduce the size of the list to 1' do
        @multi_max_path.max
        expect(@multi_max_path.size).to eq(1)
      end

      it 'should give the maximum value' do
        expect(@multi_max_path.max).to eq(14)
      end
    end
  end

  describe '#step' do
    context 'given the list is size 1' do
      it 'should raise a StopIteration Error' do
        expect { @single_max_path.step }.to raise_error(StopIteration)
      end
    end

    context 'given the list is size greater than 1' do
      it 'should remove the last row' do
        @multi_max_path.step
        expect(@multi_max_path.size).to eq(@multi_list.size - 1)
      end

      it 'should add the largest values to the row above it' do
        @multi_max_path.step
        expect(@multi_max_path.max).to eq(14)
      end
    end
  end

  describe '#size' do
    it 'returns the size of the list in MaxPathFinder' do
      expect(@multi_max_path.size).to eq(@multi_list.size)
    end
  end

  describe '#new' do
    context 'given no parameters' do
      it 'should raise an Argument Error' do
        expect { MaxPathFinder.new }.to raise_error(ArgumentError)
      end
    end

    context 'given a list of lists' do
      before :each do
        @list_size = @multi_list.size
      end

      it 'should initialize with list' do
        expect(@multi_max_path).to be_an_instance_of(MaxPathFinder)
        expect(@multi_max_path.list).to eql(@multi_list)
        expect(@multi_max_path.size).to eql(@list_size)
      end
    end
  end
end

# mpf = MaxPathFinder.new([[75],
# [95, 64],
# [17, 47, 82],
# [18, 35, 87, 10],
# [20, 04, 82, 47, 65],
# [19, 01, 23, 75, 03, 34],
# [88, 02, 77, 73, 07, 63, 67],
# [99, 65, 04, 28, 06, 16, 70, 92],
# [41, 41, 26, 56, 83, 40, 80, 70, 33],
# [41, 48, 72, 33, 47, 32, 37, 16, 94, 29],
# [53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14],
# [70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57],
# [91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48],
# [63, 66, 04, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31],
# [04, 62, 98, 27, 23, 9, 70, 98, 73, 93, 38, 53, 60, 04, 23]])
# puts mpf.max
