"""
Project Euler Problem 11

Find the greatest product of four adjacent numbers in the same direction (up,
down, left, right, or diagonally) in the 20x20 grid:

08 02 22 97 38 15 00 40 00 75 04 05 07 78 52 12 50 77 91 08
49 49 99 40 17 81 18 57 60 87 17 40 98 43 69 48 04 56 62 00
81 49 31 73 55 79 14 29 93 71 40 67 53 88 30 03 49 13 36 65
52 70 95 23 04 60 11 42 69 24 68 56 01 32 56 71 37 02 36 91
22 31 16 71 51 67 63 89 41 92 36 54 22 40 40 28 66 33 13 80
24 47 32 60 99 03 45 02 44 75 33 53 78 36 84 20 35 17 12 50
32 98 81 28 64 23 67 10 26 38 40 67 59 54 70 66 18 38 64 70
67 26 20 68 02 62 12 20 95 63 94 39 63 08 40 91 66 49 94 21
24 55 58 05 66 73 99 26 97 17 78 78 96 83 14 88 34 89 63 72
21 36 23 09 75 00 76 44 20 45 35 14 00 61 33 97 34 31 33 95
78 17 53 28 22 75 31 67 15 94 03 80 04 62 16 14 09 53 56 92
16 39 05 42 96 35 31 47 55 58 88 24 00 17 54 24 36 29 85 57
86 56 00 48 35 71 89 07 05 44 44 37 44 60 21 58 51 54 17 58
19 80 81 68 05 94 47 69 28 73 92 13 86 52 17 77 04 89 55 40
04 52 08 83 97 35 99 16 07 97 57 32 16 26 26 79 33 27 98 66
88 36 68 87 57 62 20 72 03 46 33 67 46 55 12 32 63 93 53 69
04 42 16 73 38 25 39 11 24 94 72 18 08 46 29 32 40 62 76 36
20 69 36 41 72 30 23 88 34 62 99 69 82 67 59 85 74 04 36 16
20 73 35 29 78 31 90 01 74 31 49 71 48 86 81 16 23 57 05 54
01 70 54 71 83 51 54 69 16 92 33 48 61 43 52 01 89 19 67 48

Tackling it the same way I've been tackling all these problems (at least
recently):  Piece by piece, and with the most direct way possible.
"""
import csv
import unittest
import operator
import functools

def FileReader(file_name):
    number_list = list()
    try:
        with open(file_name, 'r', encoding='utf-8') as file_obj:
            for line in file_obj:
                line = line.strip()
                line = line.split()
                line = [int(num) for num in line]
                number_list.append(line)
    except IOError:
        print("File Failed to Open!")

    return number_list


def TwoDimensionListTransposer(num_list):
    transposed = list(map(list, zip(*num_list)))
    return transposed


def PartitionSummator(num_list, groups_of):
    max_len = len(num_list)
    beg_index = 0
    end_index = groups_of
    largest_product = 0
    while end_index <= max_len:
        segment_product = functools.reduce(operator.mul, num_list[beg_index:end_index], 1)
        largest_product = max(largest_product, segment_product)
        beg_index += 1
        end_index += 1

    return largest_product


def LargestProductOfRow(twoD_numlist, groups_of):
    largest_product = 0
    for row in twoD_numlist:
        row_product = PartitionSummator(row, groups_of)
        largest_product = max(largest_product, row_product)
    return largest_product


def BottomUpDiagonalExtractor(num_list, groups_of, max_row, max_col):
    diagonal_list = []

    row_index = groups_of - 1
    col_index = 0

    while row_index < max_row:
        while col_index + groups_of <= max_col:
            temp_list = []
            for num in range(groups_of):
                temp_list.append(num_list[row_index-num][col_index+num])
            diagonal_list.append(temp_list)
            col_index += 1
        col_index = 0
        row_index += 1

    return diagonal_list


def TopBottomDiagonalExtractor(num_list, groups_of, max_row, max_col):
    diagonal_list = []

    row_index = 0
    col_index = 0

    while row_index + groups_of <= max_row:
        while col_index + groups_of <= max_col:
            temp_list = []
            for num in range(groups_of):
                temp_list.append(num_list[row_index+num][col_index+num])
            diagonal_list.append(temp_list)
            col_index += 1
        col_index = 0
        row_index += 1

    return diagonal_list


def DiagonalExtractor(num_list, groups_of):
    return_list = []

    max_row = len(num_list)
    max_col = len(num_list[0])

    bottom_up = BottomUpDiagonalExtractor(num_list, groups_of, max_row, max_col)
    top_bottom = TopBottomDiagonalExtractor(num_list, groups_of, max_row, max_col)
    return_list.extend(bottom_up)
    return_list.extend(top_bottom)

    return return_list


def FindGreatestProduct(num_list, groups_of):
    largest_product = 0

    # largest horizontal product
    largest_product = max(LargestProductOfRow(num_list, groups_of), largest_product)

    # largest vertical product
    vertical_list = TwoDimensionListTransposer(num_list)
    largest_product = max(LargestProductOfRow(vertical_list, groups_of), largest_product)

    # largest diagonal product
    diagonal_list = DiagonalExtractor(num_list, groups_of)
    largest_product = max(LargestProductOfRow(diagonal_list, groups_of), largest_product)

    return largest_product



class TestFindGreatestProduct(unittest.TestCase):
    def setUp(self):
        self.groups_of = 3
        self.simple_list = [[3,5,7],
                            [9,1,4],
                            [8,2,6]]

        self.intermediate_list =    [[37, 49, 26, 78, 36],
                                    [59, 58, 76, 80, 29],
                                    [19, 99, 43, 24, 82],
                                    [57, 7, 82, 32, 39],
                                    [31, 78, 98, 79, 20]]

    def test_HorizontalRowSummationSimple_105(self):
        result = LargestProductOfRow(self.simple_list, self.groups_of)
        self.assertEqual(result, 105)

    def test_HorizontalRowSummationInterm_603876(self):
        result = LargestProductOfRow(self.intermediate_list, self.groups_of)
        self.assertEqual(result, 603876)

    def test_ListTransposer(self):
        result = TwoDimensionListTransposer(self.simple_list)
        self.assertEqual(result, [[3,9,8], [5,1,2], [7,4,6]])

    def test_VerticalRowSummationSimple_216(self):
        transposed = TwoDimensionListTransposer(self.simple_list)
        result = LargestProductOfRow(transposed, self.groups_of)
        self.assertEqual(result, 216)

    def test_VerticalRowSummationInterm_345548(self):
        transposed = TwoDimensionListTransposer(self.intermediate_list)
        result = LargestProductOfRow(transposed, self.groups_of)
        self.assertEqual(result, 345548)

    def test_DiagonalReshaperSimple(self):
        result = DiagonalExtractor(self.simple_list, self.groups_of)
        self.assertEqual(result, [[8,1,7],[3,1,6]])

    def test_DiagonalReshaperInterm(self):
        result = DiagonalExtractor(self.intermediate_list, 3)
        self.assertEqual(result,    [[19, 58, 26],[99, 76, 78], [43, 80, 36],
                                    [57, 99, 76], [7, 43, 80], [82, 24, 29],
                                    [31, 7, 43], [78, 82, 24], [98, 32, 82],
                                    [37, 58, 43], [49, 76, 24], [26, 80, 82],
                                    [59, 99, 82], [58, 43, 32], [76, 24, 39],
                                    [19, 7, 98], [99, 82, 79], [43, 32, 20]])

    def test_DiagonalProductsSimple_56(self):
        diagonal_lists = DiagonalExtractor(self.simple_list, self.groups_of)
        result = LargestProductOfRow(diagonal_lists, self.groups_of)
        self.assertEqual(result, 56)

    def test_DiagonalProductsInterm_641322(self):
        diagonal_lists = DiagonalExtractor(self.intermediate_list, self.groups_of)
        result = LargestProductOfRow(diagonal_lists, self.groups_of)
        self.assertEqual(result, 641322)

    def test_Problem11_ProjectEuler_70600674(self):
        number_list = FileReader("problem_11_data.txt")
        result = FindGreatestProduct(number_list, 4)
        self.assertEqual(result, 70600674)


if __name__ == '__main__':
    # unittest.main()
    twoD_numlist = FileReader("problem_11_data.txt")
    num_list = [sublist for sublist in twoD_numlist for item in sublist]
    print(num_list)