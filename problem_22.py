"""
Find total name scores in file "p022_names.txt"

Sort names in file.

For each name
    For each letter
        Convert letter to positional value
        Add value to running total for this name
    Multiply name total with position of name in sorted list
    Add multiplied total to overall total.
Return total
"""
# For index function that returns index of character in alphabet
import string

def get_char_index(char):
    return string.uppercase.index(char.upper()) + 1

def get_name_value(name):
    name_value = 0
    for char in name:
        name_value += get_char_index(char)

    return name_value

def get_name_position(name, sorted_name_list):
    return sorted_name_list.index(name) + 1


# Open file, process, and sort names
name_file = open("p022_names.txt", 'r')
raw_names = name_file.read()
name_file.close()
unsorted_names = raw_names.split(',')
stripped_names = [name.strip('\"') for name in unsorted_names]
sorted_names = sorted(stripped_names)

# Get name scores of each name, and sum the product of position and
# individual scores
total_name_score = 0
for name in sorted_names:
    name_value = get_name_value(name)
    name_position = get_name_position(name, sorted_names)
    total_name_score += (name_position * name_value)
print(total_name_score)
