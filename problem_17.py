"""
Project Euler Problem 17

Count the number of characters used if a count from 1 to 1000
was done using words.

Spaces and Hyphens aren't included.
The word "and" is included.

Need to represent all numbers in words.
    One, two, three.. nine
    Ten, twenty, thirty.. ninety
    One hundred, two hundred, three hundred.. nine hundred
    One thousand


Might consider using three (or four) separate dictionaries
that represent each digit.

Breakdown each number into its individual components:

274 --> 200 + 70 + 4 --> Two hundred and seventy four
317 --> 300 + 10 + 7 --> Three hundred and seventeen.

Values from ten to twenty are a special case.
Values in the hundreds have an additional and.

May not even need to represent them in words, we just need
the count.

1 --> 3
20 --> twenty --> 6
132 --> 100 + 30 + 2 --> one hundred and thirty two -->
    10 + 3 + 6 + 3 = 22

In this way, we can save the over head of generating actual
words, then calculating those words; instead, we'll just
count those words ahead of time, and go with simple addition.

Need to organize the value such that it shaves the number as
it progresses down the program.

Essentials:
    for num in range(1, 1000):
        total += number_letter_counter(num)
    return total

So we basically need a function that takes integers, and returns
the letter count for that one number.
"""

import unittest
import ddt

# one two three four five six seven eight nine
# ten eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen
# twenty seven

ones = {0: 0, 1: 3, 2: 3, 3: 5, 4: 4, 5: 4, 6: 3, 7: 5, 8: 5, 9: 4}
teens = {10: 3, 11: 6, 12: 6, 13: 8, 14: 8, 15: 7, 16: 7, 17: 9, 18: 8, 19: 8}
tens = {20: 6, 30: 6, 40: 5, 50: 5, 60: 5, 70: 7, 80: 6, 90: 6}
thousand = 8
hundred = 7
and_word = 3


def number_letter_counter(number):
    total = 0
    tho_val = number // 1000
    number -= tho_val * 1000

    hun_val = number // 100
    number -= hun_val * 100

    ten_val = (number // 10) * 10
    number -= ten_val

    one_val = number

    if tho_val > 0:
        total += ones[tho_val] + thousand

    if hun_val > 0:
        # if it's exactly 100's, i.e. 100, 200, 300; no "and"
        if not (ten_val == 0 and one_val == 0):
            total += and_word  # and == 3 letters

        total += ones[hun_val] + hundred

    if ten_val >= 20:
        total += tens[ten_val]
        total += ones[one_val]
    # if values in range [10,19], inclusive.
    elif ten_val == 10:
        total += teens[ten_val + one_val]
    else:
        total += ones[one_val]

    return total


def total_number_letters(number):
    total = 0
    for num in range(1, number + 1):
        total += number_letter_counter(num)
    return total


@ddt.ddt
class TestNumberLetterCounts(unittest.TestCase):

    @ddt.file_data("problem_17_test.json")
    def test_number_letter_counter(self, value):
        input_val, expected = value
        actual = number_letter_counter(input_val)
        self.assertEqual(actual, expected)

    @ddt.data((5, 19), (9, 36), (10, 39), (20, 112), (30, 208), (1000, 21124))
    def test_total_number_letters(self, value):
        input_val, expected = value
        actual = total_number_letters(input_val)
        self.assertEqual(actual, expected)

if __name__ == '__main__':
    unittest.main()
    # print(total_number_letters(1000))
