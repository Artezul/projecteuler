"""
Project Euler Problem 15

How many routes are possible in a 20 by 20 grid.

Possibilities:
    Represent n by n grids as an (n + 1) by (n + 1) 2D array.

    How to represent all possible combinations?

    2 x 2 = 6

    1*2 + 2*2 = 6

    0,0 0,1 0,2
    1,0 1,1 1,2
    2,0 2,1 2,2

    3 x 3 = 20

    1*2 + 2*2 + 3*2 + 3*2 + 1*2 = 20

    0,0 0,1 0,2 0,3
    1,0 1,1 1,2 1,3
    2,0 2,1 2,2 2,3
    3,0 3,1 3,2 3,3

    Hypothesis 1 (nope):

    4 x 4 = 70
    1*2 + 2*2 + 3*2 + 4*2 + 4*2 + 3*2 + 2*2 + 2*2

    0,0 0,1 0,2 0,3 0,4
    1,0 1,1 1,2 1,3 1,4
    2,0 2,1 2,2 2,3 2,4
    3,0 3,1 3,2 3,3 3,4
    4,0 4,1 4,2 4,3 4,4

    We begin with a single traveler at (0,0).  We check if there are two paths
    with which we can enter.

    Two separate for loops:
    One going up to "n", the other going down from "n - 1" but not including 1.

    Hypothesis 2 (nope):
    2x2 = 2**2 + 2*1 = 6
    3x3 = 2**4 + 2*2 = 20
    4x4 = 2**6 + 2*3 = 70

    Solution:

    While looking for confirmation that 4x4 = 70, accidently glimpsed the
    algorithm for solving this problem.  To my utter disdain, I deprived myself
    of the joy of solving it myself.  As expected, the algorithm was so simple
    compared to my attempts at solving this problem.

    1
    1 1
    1 2 1
    1 3 3 1
    1 4 6 4 1
    5 10 10 5
    15 20 15
    35 35
    70

    It seems each successive row is the sum of two adjacent elements of a
    previous row.
"""

import unittest
from ddt import ddt, data


def lattice_tophalf(grid_size):
    prev = [1]
    for num in range(grid_size):
        curr = []
        for col in range(len(prev)):
            if col == 0:
                curr.append(1)
            else:
                curr.append(prev[col - 1] + prev[col])
        curr.append(1)
        prev = curr

    return prev


def lattice_bothalf(top_half):
    length = len(top_half) - 1
    for num in range(length, 0, -1):
        curr = []
        # list shrinks with each iteration
        for col in range(length):
            curr.append(top_half[col] + top_half[col + 1])
        length -= 1
        top_half = curr

    return sum(top_half)


def lattice_paths(grid_size):
    top_half = lattice_tophalf(grid_size)
    return lattice_bothalf(top_half)


@ddt
class TestLatticePaths(unittest.TestCase):
    @data(([1, 2, 1], 6), ([1, 3, 3, 1], 20), ([1, 4, 6, 4, 1], 70))
    def test_lattice_bothalf(self, value):
        input_val, expected = value
        actual = lattice_bothalf(input_val)
        self.assertEqual(expected, actual)

    @data((1, [1, 1]), (2, [1, 2, 1]), (3, [1, 3, 3, 1]))
    def test_lattice_tophalf(self, value):
        input_val, expected = value
        actual = lattice_tophalf(input_val)
        self.assertEqual(expected, actual)


if __name__ == '__main__':
    unittest.main()
    # print(lattice_paths(20))
