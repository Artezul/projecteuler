"""
Find the nth Fibonacci Number that has 1000 digits.
"""


def FibonacciGenerator(max_int):
    prev_num = 1
    curr_num = 1
    while curr_num <= max_int:
        yield curr_num
        temp_num = prev_num
        prev_num = curr_num
        curr_num += temp_num


if __name__ == '__main__':
    fibs = FibonacciGenerator(10**1000)
    first = 0
    count = 1
    for fib in fibs:
        if len(str(fib)) >= 1000:
            first = fib
            break
        count += 1

    # + 1 for early break in loop
    print(count + 1)
