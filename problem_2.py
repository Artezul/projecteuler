"""
Problem 2 from Project Euler:

Find the sum of all even numbers of the Fibonacci Sequence.
"""

import unittest

def DumbSumEvenFibonacci(max_int):
    """
    This function uses a brute force method of solving the problem.

    We will iterate from 1 to the max_int; as we go along, we will
    only add/append even numbers into a list.

    Once the end of the loop has been reached, we will sum the list
    of integers, and return the resulting value.
    """
    even_list = []

    for num in FibonacciGenerator(max_int):
        if not num % 2:
            even_list.append(num)

    return sum(even_list)


def FibonacciGenerator(max_int):
    prev_num = 1
    curr_num = 1
    while curr_num <= max_int:
        yield curr_num
        temp_num = prev_num
        prev_num = curr_num
        curr_num += temp_num


class TestEvenFibonacciSum(unittest.TestCase):
    def setUp(self):
        self.base_int = 1
        self.small_int = 10
        self.max_int = 4000000
        self.test_seq = []

    def test_FibSeqTo1(self):
        correct_seq = [1]
        for num in FibonacciGenerator(1):
            self.test_seq.append(num)

        self.assertEqual(correct_seq, self.test_seq)

    def test_FibSeqTo2(self):
        correct_seq = [1, 2]

        for num in FibonacciGenerator(2):
            self.test_seq.append(num)

        self.assertEqual(correct_seq, self.test_seq)

    def test_FibSeqTo3(self):
        correct_seq = [1, 2, 3]

        for num in FibonacciGenerator(4):
            self.test_seq.append(num)

        self.assertEqual(correct_seq, self.test_seq)


    def test_FibSeqTo21(self):
        correct_seq = [1, 2, 3, 5, 8, 13, 21]
        test_seq = []
        for num in FibonacciGenerator(21):
            test_seq.append(num)

        self.assertEqual(correct_seq, test_seq)


    def test_MaxInt10_Sum10(self):
        even_sum = DumbSumEvenFibonacci(self.small_int)
        self.assertEqual(even_sum, self.small_int)

    def test_MaxInt4mil_Sum4613732(self):
        even_sum = DumbSumEvenFibonacci(self.max_int)
        self.assertEqual(even_sum, 4613732)

if __name__ == '__main__':
    unittest.main()
