This repository is the collection of my attempts/solutions to the problems
on ProjectEuler.  Many of them rely on brute force methods such as
iterating through all possibilities; other solutions have been refined thanks
to the insight gained from having seen the attempts of others through the
ProjectEuler forum posts that are made available after solving the problem.

The problems of ProjectEuler seem to help bridge a mental gap for me between
my (laughable) mathematical foundations, and my programming.  Through solving
these problems, I've come to appreciate how much more optimal using math can
be in comparison to purely code.  I've also learned to attempt these problems
with a focus on solving them first, and refactoring them for efficiency after.
This type of thinking has helped improve my capability of solving these (and
hopefully other) problems by worrying about optimization after, and not before.
It sounds almost ridiculous to consider putting optimization on the backburner,
but attempting to both solve for the solution of a problem, and optimizing the
solution seems to only compound the time needed to finish.

Also, it's kind of fun.

I never thought there'd be a point in my life as a student programmer where
I'd miss solving "textbook" code problems, but it's come to a point in my
academic career where I'm constantly bombarded with concerns of UX, UML,
security, APIs, and milestones.  The notion of having to simply solve a
problem, with the only metric being efficiency (below a minute to run),
is very liberating.
