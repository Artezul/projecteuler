"""
Problem 4 from Project Euler:

A palindromic number reads the same both ways.  The largest palindrome
made from the product of two 2-digit numbers is 9009 = 91 * 99.

Find the largest palindrome made from the product of two 3-digit numbers.

Largest number possible from the product of 2 three digit numbers is
999 * 999 = 998001. The largest palindromic number "should" be 997799.
It doesn't necessarily have 2 three digit number as its product, but at
least this gives us a starting point.

Would it be simpler to go down the list from 998001 looking for palindromes?
    Meaning there are 899 * 899 = 808201 possible combinations.

Would it be simpler to simply check all palindromic numbers instead, and
derive factors from them?
    Largest palindromic number is 899998.
    Counting down the palindromic numbers:
        999999
        998899
        997799
        996699
        995599
        994499
        993399
        99999

    We need only create half the numbers starting from 999, and count
    downwards.  We'd essentially only have a total of 899 numbers to
    check.


What's the best way to determine if it's a palindrome?
    If it's an even number, does one half equal the other?
    If it's an odd number, exluding the middle, does one half equal the other?

Test Cases:
    Is it a palindrome?

"""
import math
import unittest

# =================================================

def prime_factorization(number):
    limit = int(math.sqrt(number))
    factor = 2
    factor_list = []
    while limit > 1 and number >= limit:
        if number % factor == 0:
            number = number // factor
            factor_list.append(factor)
            factor -= 1
        factor += 1

    if number != 1:
        factor_list.append(number)

    return factor_list

# -------------------------------------------------

# class TestPrimeFactorization(unittest.TestCase):
#     def test_BaseCase1_Result1(self):
#         result = prime_factorization(1)
#         self.assertEqual(result, [1])

#     def test_BaseCase2_Result2(self):
#         result = prime_factorization(2)
#         self.assertEqual(result, [2])

#     def test_BaseCase3_Result3(self):
#         result = prime_factorization(3)
#         self.assertEqual(result, [3])

#     def test_DoubleFactor4_Result_22_(self):
#         result = prime_factorization(4)
#         self.assertEqual(result, [2,2])

#     def test_DoubleFactor9_Result_33_(self):
#         result = prime_factorization(9)
#         self.assertEqual(result, [3,3])

#     def test_MultiFactor72_Result_22233_(self):
#         result = prime_factorization(72)
#         self.assertEqual(result, [2,2,2,3,3])


# =================================================
# =================================================

def palindrome_checker(number):
    str_num = str(number)
    str_len = len(str_num)
    IS_PALIND = True

    if str_len < 2:
        IS_PALIND = False

    begin = 0
    endin = str_len - 1 
    char_limit = str_len // 2

    while IS_PALIND and char_limit > begin:
        if str_num[begin] != str_num[endin]:
            IS_PALIND = False
        else:
            begin += 1
            endin -= 1

    return IS_PALIND

# -------------------------------------------------

# class TestPalindromeChecker(unittest.TestCase):
#     def test_InvalidPalindrome1(self):
#         result = palindrome_checker(1)
#         self.assertFalse(result)

#     def test_InvalidPalindrome9(self):
#         result = palindrome_checker(9)
#         self.assertFalse(result)

#     def test_EvenSize_ValidPalindrome11(self):
#         result = palindrome_checker(11)
#         self.assertTrue(result)

#     def test_EvenSize_ValidPalindrome55(self):
#         result = palindrome_checker(55)
#         self.assertTrue(result)

#     def test_OddSize_ValidPalindrome111(self):
#         result = palindrome_checker(111)
#         self.assertTrue(result)

#     def test_OddSize_ValidPalindrome55555(self):
#         result = palindrome_checker(55555)
#         self.assertTrue(result)

#     def test_OddSize_ValidPalindrome1234321(self):
#         result = palindrome_checker(1234321)
#         self.assertTrue(result)

# =================================================

def generate_upper_limit_of_length(num_len):
    number = 0
    for num in range(num_len):
        number += 9 * pow(10, num)

    return number 

def generate_lower_limit_of_length(num_len):
    return pow(10, num_len - 1)

def palindrome_generator(palin_len):
    if palin_len < 2:
        raise ValueError("Value must be greater than or equal to 2")

    palin_half_len = palin_len // 2

    upper_palin_num = generate_upper_limit_of_length(palin_half_len)

    # Subtract one so the lowest palindrome is included in the loop.
    lower_palin_num = generate_lower_limit_of_length(palin_half_len) - 1

    for num in range(upper_palin_num, lower_palin_num, -1):
        left_half_palin = str(num)
        right_half_palin = "".join(left_half_palin[-1::-1])
        if palin_len % 2 == 0:
            yield left_half_palin + right_half_palin
        else:
            for num in range(9, -1, -1):
                yield left_half_palin + str(num) + right_half_palin



# class TestPalindromeGenerator(unittest.TestCase):
#     def test_SmallestNumberOfLength5(self):
#         result = generate_lower_limit_of_length(5)
#         self.assertEqual(result, 10000)

#     def test_SmallestNumberOfLength2(self):
#         result = generate_lower_limit_of_length(2)
#         self.assertEqual(result, 10)

#     def test_LargestNumberOfLength5(self):
#         result = generate_upper_limit_of_length(5)
#         self.assertEqual(result, 99999)

#     def test_LargestNumberOfLength2(self):
#         result = generate_upper_limit_of_length(2)
#         self.assertEqual(result, 99)

#     def test_ValidPalindromeLength2(self):
#         gen = palindrome_generator(2)
#         expected = [str(11 * num) for num in range(9, 0, -1)]
#         actual = [num for num in gen]
#         self.assertEqual(expected, actual)

#     def test_ValidPalindromeLength3(self):
#         gen = palindrome_generator(3)
#         upper_palindromes = [999 - num for num in range(0, 909, 101)]
#         expected = []
#         for num_upper in upper_palindromes:
#             for num_inner in range(0, 100, 10):
#                 expected.append(str(num_upper - num_inner))
#         actual = [num for num in gen]
#         expected.sort()
#         actual.sort()
#         self.assertEqual(expected, actual)

def find_two__three_digit_multiples_of_palindrome():
    palin_gen = palindrome_generator(6)
    candidate_list = []
    for palin in palin_gen:
        factor_list = prime_factorization(int(palin))
        count = 0
        for prime in factor_list:
            if math.log(prime, 10) < 3:
                count += 1
            else:
                count = 0

        if count >= 2:
            candidate_list.append((palin, factor_list))

    return candidate_list

if __name__ == '__main__':
    """
    This is a poignant reminder of how much time I waste trying to optimize.
    I go about creating multiple functions in an attempt to be efficient, but
    when I hit a dead end, I go for the obvious solution.

    If I reverse this process, I could've saved myself a day of effort.

    Use this program as a reminder:  PLAY TO YOUR CONSTRAINTS.

    These Project Euler problems don't have memory or performance limitations.
    Also, the computer's speed with which it is able to compute numbers, it is
    entirely feasible to use the "bone head" method of solving this problem.

    I had thought we could reduce the problem size by merely generating all of
    the possible palindromes that could be created with 3 digit numbers.
    """
    candidate = []
    for num_outer in range(999, 99, -1):
        for num_inner in range(999, 99, -1):
            number = num_outer * num_inner
            if palindrome_checker(number):
                candidate.append((number, (num_outer, num_inner)))

    import pprint
    candidate.sort()
    pprint.pprint(candidate)



