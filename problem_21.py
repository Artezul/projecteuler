"""
Find the sum of all amicable numbers under 10000.

What is an amicable number?

Component needed to explain amicable number:
    Let some function d(n) be defined as the sum of all proper divisors of n.

    Proper divisor:  Numbers that are less than n which divide evenly into n.

Amicable Number:
    If d(a) = b and d(b) = a, and a != b, then both a and b are an amicable
    pair, and are both amicable numbers.
"""

import unittest
import ddt

def find_all_proper_divisors_of(n):
    import math
    divisor_list = list()
    # Divisors greater than half the value (n) don't exist
    # sqrt better, but causes problems.
    upper_limit = int(math.ceil(n / 2.0)) + 1
    for divisor in range(1, upper_limit):
        if n % divisor == 0:
            divisor_list.append(divisor)

    return divisor_list

def sum_of_proper_divisors(divisor_list):
    return sum(divisor_list)

def check_for_amicable_pair(value):
    divisor_list = find_all_proper_divisors_of(value)
    amicable_candidate = sum_of_proper_divisors(divisor_list)
    # Did it quick and sloppy
    if sum_of_proper_divisors(find_all_proper_divisors_of(amicable_candidate)) == value and \
        amicable_candidate != value:
        return [value, amicable_candidate]
    else:
        return []

def find_sum_for_all_amicable_numbers_below(value):
    result = set()
    for number in range(2, value + 1):
        result.update(check_for_amicable_pair(number))
    return sum(result)

@ddt.ddt
class TestProblem21(unittest.TestCase):

    @ddt.data((2,[1]), (3, [1]), (4, [1, 2]), (5, [1]), (6, [1, 2, 3]))
    def test_find_all_proper_divisors_of_input_val(self, value):
        # Unpack ddt values
        input_val, expected = value
        actual = find_all_proper_divisors_of(input_val)
        self.assertEqual(actual, expected)

    @ddt.data(([1, 2], 3), ([1, 2, 3], 6), ([1, 2, 3, 4, 6], 16))
    def test_find_sum_of_proper_divisors(self, value):
        input_val, expected = value
        actual = sum_of_proper_divisors(input_val)
        self.assertEqual(actual, expected)

    @ddt.data((220, [220, 284]), (111, []), (6, []), (28, []))
    def test_check_for_amicable_pair(self, value):
        input_val, expected = value
        actual = check_for_amicable_pair(input_val)
        self.assertEqual(actual, expected)

    @ddt.data((10000, 31626))
    def test_find_sum_for_all_amicable_numbers(self, value):
        input_val, expected = value
        actual = find_sum_for_all_amicable_numbers_below(input_val)
        self.assertEqual(actual, expected)

if __name__ == '__main__':
    unittest.main()
