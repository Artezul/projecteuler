"""
Problem 3 from Project Euler.
Find the greatest prime factor for the number 600851475143.
"""
import unittest
import math

def time_it(func):
    def func_wrapper(number):
        import time
        start = time.time()
        print(func.__name__)
        print("Factors Found: {}".format(func(number)))
        print("Seconds: {0:.2f}".format(time.time() - start))
    return func_wrapper

def GetFactorLimit(number):
    return int(math.sqrt(number))

@time_it
def PrimeFactorization1(number):
    """
    Something is causing it to slow down when it the number is around 6.83E16.

    Still can't quite figure it out, but there is no arguing that the "brute force"
    method of solving this problem was actually quicker (with a sqrt tweak).
    """
    import time
    start = time.time()
    factor_limit = GetFactorLimit(number) + 1
    possible_factors = list(range(factor_limit, 1, -2))
    factors_found = list()
    possible_factor = 1
    print(time.time() - start)
    start = time.time()
    while possible_factors and possible_factor <= number:
        possible_factor = possible_factors.pop()
        IS_FACTOR = False if number % possible_factor else True

        if IS_FACTOR:
            factors_found.append(possible_factor)
            number /= possible_factor
            possible_factors.append(possible_factor)
    print(time.time() - start)
    factors_found.append(number)
    return factors_found

    # ---------------------------------------------------------------------------------

# @time_it
def PrimeFactorization2(number):
    """
    While there is redundancy in traversing numbers that aren't prime (like all even numbers),
    it is still supremely quick.

    Attempts to "trim" numbers multiples or even numbers slow the program down.  While the benefit
    of not traversing redundant values is evident, the overhead far outweigh the benefits it seems.
    """
    divisor = 2
    factor_limit = int(math.sqrt(number))
    factor_list = list()

    while number > 1 and divisor <= factor_limit:
        if not number % divisor:
            factor_list.append(divisor)
            number /= divisor
            factor_limit = int(math.sqrt(number))
            divisor -= 1
        divisor += 1

    factor_list.append(number)
    return factor_list




class TestPrimeFactorization(unittest.TestCase):
    def test_PrimeFactorization4_list2_2(self):
        result = PrimeFactorization2(4)
        self.assertEqual(result, [2,2])

    def test_PrimeFactorization6_list2_3(self):
        result = PrimeFactorization2(6)
        self.assertEqual(result, [2,3])

    def test_PrimeFactorization10_list2_5(self):
        result = PrimeFactorization2(10)
        self.assertEqual(result, [2,5])

    def test_PrimeFactorization21_list3_7(self):
        result = PrimeFactorization2(21)
        self.assertEqual(result, [3,7])

    def test_PrimeFactorization132_list2_2_3_11(self):
        result = PrimeFactorization2(132)
        self.assertEqual(result, [2, 2, 3, 11])

    def test_GetFactorLimit1_1Limit(self):
        result = GetFactorLimit(1)
        self.assertEqual(result, 1)

    def test_GetFactorLimit2_1Limit(self):
        result = GetFactorLimit(2)
        self.assertEqual(result, 1)

    def test_GetFactorLimit4_2Limit(self):
        result = GetFactorLimit(4)
        self.assertEqual(result, 2)

if __name__ == '__main__':
    # unittest.main()
    large_prime = 375371 * 611953 * 297503
    # print(PrimeFactorization1(large_prime))
    print(PrimeFactorization2(large_prime))
    # print(PrimeFactorization(781002970380700261))