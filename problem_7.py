"""
Project Euler Problem 7 - August 12th 2015
What is the 10001st prime number?

Going to "sift" through all the prime numbers.

A while loop that continues until it reaches a count of 100001 primes.

Inside the loop, is another counter that will increment with each loop,
representing the next number to be displayed.

A function will test whether or not this number is prime.

If it is prime, increment the prime counter.

Regardless of whether it's prime or not, increment the number counter.
"""
import math
import unittest

def isPrime(number):
    """
    Optimized for her pleasure?
    """
    if number < 2:
        return False

    if number > 1 and number < 4:
        return True

    if number % 2 == 0 or number % 3 == 0:
        return False

    if number < 9:
        return True

    limit = int(math.floor(math.sqrt(number)))

    for num in range(5, limit + 1, 6):
        if number % num == 0:
            return False

        if (number) % (num + 2) == 0:
            return False

    return True

# def isPrime(number):
#     """
#     limit:      Represents the maximum value we will use to test for factors.
#     factor:     Represents the current number we are attempting to divide the
#                     number by.
#     is_prime:   Represents the boolean which we will be returning indicating
#                     that the value is either a prime or not.

#     """
#     limit = math.sqrt(number)
#     factor = 2.0
#     is_factorable = False

#     while limit >= factor and not is_factorable:
#         if number % factor == 0:
#             is_factorable = True 
#         else:
#             factor += 1.0

#     return not is_factorable 

def findThe_ith_Prime(ith_prime):
    prime_counter = 1
    ret_val = 0
    number = 1
    while prime_counter < ith_prime:
        number += 2
        if isPrime(number):
            prime_counter += 1

    return number



class TestPrimeFinder(unittest.TestCase):
    def test_ValidPrime2_True(self):
        self.assertTrue(isPrime(2))

    def test_ValidPrime3_True(self):
        self.assertTrue(isPrime(3))

    def test_InvalidPrime4_False(self):
        self.assertFalse(isPrime(4))

    def test_InvalidPrime6_False(self):
        self.assertFalse(isPrime(6))

    def test_Return6thPrime_13(self):
        result = findThe_ith_Prime(6)
        self.assertEqual(result, 13)

    def test_Return3rdPrime_5(self):
        result = findThe_ith_Prime(3)
        self.assertEqual(result, 5)

    def test_Return51stPrime_229(self):
        result = findThe_ith_Prime(50)
        self.assertEqual(result, 229)


"""
The __name__ variable is a special variable that is set whenever the
python interpreter is executing a script.  If this script is being
executed directly, then the __name__ variable will be set to main,
and as such, allows us to define how it will behave.  If it is run
as a module, then this if statement is ignored.
"""
if __name__ == '__main__':
    # unittest.main()
    print(findThe_ith_Prime(1000000))