import unittest

from datetime import date
from datetime import timedelta

"""
Trying to reverse engineer either a more compartmentalized program, or
a more inefficient and verbose one.  So far, it's easier to do the latter..
Big surprise.
"""

def first_algorithm():
    sunday_count = 0
    start_date = date(1901, 1, 1)

    while(start_date != date(2001, 1, 1)):
        start_date += timedelta(days=1)

        if start_date.day == 1 and start_date.isoweekday() == 7:
                sunday_count += 1

    return sunday_count

def second_algorithm():
    sunday_count = 0
    num_days_in_month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    for year in range(1901, 2001):
        for month in range(0, 12):
            for day in range(0, num_days_in_month[month]):
                try:
                    start_date = date(year, month + 1, day + 1)
                    if start_date.day == 1 and start_date.isoweekday() == 7:
                        sunday_count += 1
                except ValueError:
                    print("Invalid Date: {}/{}/{}".format(month, day, year))

    return sunday_count

def third_algorithm():
    sunday_count = 0
    for year in range(1901, 2001):
        for month in range(0, 12):
            # We're only concerned with the first of the month
            day = 1
            start_date = date(year, month + 1, day)
            if start_date.day == 1 and start_date.isoweekday() == 7:
                print(start_date)
                sunday_count += 1
    return sunday_count

def fourth_algorithm():
    """
    There must be some clever way of accomplishing this.  In the end,
    we're only interested in days with which it is the first of the
    month, and it is a Sunday.  We're given the number of days in a
    month with which to possible predict the day without relying on
    external libraries.
    1: Monday
    7: Sunday
    14: Sunday
    21: Sunday
    28: Sunday
    31: Wednesday
    32 or 1: Thursday

    Huh.  A tricky way would be to simply add a sum of days to a running
    total, and take a modulus of 7 to determine whether or not it is a
    Sunday.

    09-01-1901 is a Sunday.
    January: 31
    February: 28
    March: 31
    April: 30
    May: 31
    June: 30
    July: 31
    August: 31
    September: 30

    31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 1

    Hm, unfortunately, such a realization will only result in a Big-O of n**2,
    the same efficiency as the third algorithm.
    """
    pass


def TotalFirstOfMonthSundays():
    return third_algorithm()
    # return second_algorithm()
    # return first_algorithm()

class TestTotalFirstOfMonthSundays(unittest.TestCase):
    def test_TotalFirstOfMonthSundays_01011900_12312000(self):
        expected = 171
        actual = TotalFirstOfMonthSundays()
        self.assertEqual(expected, actual)

if __name__ == '__main__':
    unittest.main()
