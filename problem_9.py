"""
Project Euler Problem 9 - August 13, 2015

Find the product 'abc' such that a + b + c = 1000, and a**2 + b**2 = c**2.

Simple:
    Iterate through all combinations of the values 30 x 30, take the square
    root of the result, and see if it's a valid integer.
    If the result (c) is a valid integer, then sum the sides to see if it
    equals to the value 1000.

I do feel guilty doing things this way, because it seems so.. unintelligent?

Took a blind stab and felt that whatever the values where, a and b couldn't
possibly exceed 500.  From there, just brute force.

However, doing it this way cost me 13 minutes of my time, whereas before it
would probably take me the better part of 8 hours.  So there's that.
"""
import math

def findPythagTripletThatSumsTo(number):
    for a in range(2, number // 3):
        for b in range(2, number // 2):
            c_squared = pow(a, 2) + pow(b, 2)
            c = math.sqrt(c_squared)
            if c == math.floor(c):
                if a + b + c == number:
                    print("A: {}\nB: {}\nC: {}".format(a, b, c))
                    print()


if __name__ == '__main__':
    findPythagTripletThatSumsTo(10000)