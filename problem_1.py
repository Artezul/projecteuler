"""
Problem 1 from projecteuler.net:

If we list all the natural numbers below 10 that are multiples of 3 or 5, we
get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.
"""

import unittest


def MultThreeFiveSum(max_int):
    """
    A more brute force method.  I check the entire range from
    0 to max_int for multiples of Three or Five.
    """
    total = 0
    for num in range(max_int):
        if not (num % 3) or not (num % 5):
            total += num

    return total


def MultSum(mult, max_int):
    """
    Refactored recurring code in AltMulThreeFiveSum, and
    shoved it into its own little method.
    """
    total = 0
    index = 0
    product = 0

    while product < max_int:
        total += product
        index += 1
        product = index * mult

    return total


def AltMultThreeFiveSum(max_int):
    """
    Perhaps less intuitive, but it's more efficient?

    In this case, I merely find the largest multiple of 3 or
    5, and sum up the product from the product of those iterations.

    Just eye-balling it:  333 + 199 + 66 iterations versus the mandatory
    1000.  467 iterations were shaved off as a result.

    As a note to myself:  Typically the original method would be
    favorable in terms of maintainability or readiability; however,
    I can't deny the improvement in efficiency allotted by this
    alternate method -as such, it's at the very least worth looking at.
    """
    product_three = MultSum(3, max_int)
    product_five = MultSum(5, max_int)
    product_fifteen = MultSum(15, max_int)

    return product_three + product_five - product_fifteen


class TestSumOfAll_ThreesAndFives(unittest.TestCase):
    def test_SumOfAllThreesAndFives_BelowTen(self):
        user_sum = AltMultThreeFiveSum(10)
        true_sum = 3 + 6 + 9 + 5

        self.assertEqual(user_sum, true_sum)

    def test_SumOfAllThreeAndFives_BelowTwenty(self):
        user_sum = AltMultThreeFiveSum(20)
        true_sum = 3 + 5 + 6 + 9 + 10 + 12 + 15 + 18

        self.assertEqual(user_sum, true_sum)

    def test_SumOfAllThreeAndFives_BelowThousand(self):
        user_sum = AltMultThreeFiveSum(1000)
        true_sum = 233168

        self.assertEqual(user_sum, true_sum)


if __name__ == '__main__':
    unittest.main()
