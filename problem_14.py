"""
Project Euler Problem 14

Find the starting number, under one million, that produces the longest
collatz sequence.

n / 2 if n is even
3n + 1 if n is odd

A loop that checks if the number is reached 1.
A condition statement of whether it's odd or even.
A result that shows both the starting number and chain count.

We feed it a number from 1 to 1 million.
    For each number, calculate the collatz sequence.
    For each iteration, count.
After each loop, keep the largest count, and starting number.
Repeat until 1 million is reached.
return result of starting number and sequence count.
"""

import unittest
from ddt import ddt, data

def CollatzSequence(number):
    count = 1

    if number == 0:
        raise ValueError()

    while number != 1:
        if number % 2 == 0:
            number /= 2
        else:
            number = 3 * number + 1
        count += 1

    return count

def LongestCollatzSequence(number):
    # (starting number, sequence count)
    largest = (0,0)
    for num in range(1, number):
        count = (num, CollatzSequence(num))
        largest = count if largest[1] < count[1] else largest

    return largest

@ddt
class TestLongestCollatzSequence(unittest.TestCase):
    @data((2,(2,2)), (6,(6,9)))
    def test_LongestCollatzSequence(self, value):
        input_val, expected = value
        actual = LongestCollatzSequence(input_val)
        self.assertEqual(actual, expected)


    @data(0)
    def test_CollatzException(self, value):
        self.assertRaises(ValueError, CollatzSequence, value)

    @data((2, 2), (3, 8), (6, 9), (1,1))
    def test_CollatzSequence(self, value):
        input_val, expected = value
        actual = CollatzSequence(input_val)
        self.assertEqual(expected, actual)

if __name__ == '__main__':
    # unittest.main()
    print(LongestCollatzSequence(1000000))