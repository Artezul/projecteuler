"""
Project Euler Problem 16

Find the sum of the digits of the number 2^1000

Steps:
    Generate the value 2^1000.
    Convert it into a string.
    Iterate through the string
        Sum each value
"""

import unittest
from ddt import ddt, data


def generate_exponent_base2(exponent):
    return pow(2, exponent)


def convert_integer_to_string(value):
    return str(value)


def create_sum_from_string(text):
    return sum([int(char) for char in text])


def power_digit_sum(number):
    integer = generate_exponent_base2(number)
    string = convert_integer_to_string(integer)
    return create_sum_from_string(string)


@ddt
class TestPowerDigitSum(unittest.TestCase):

    @data((15, 26), (5, 5))
    def test_power_digit_sum(self, value):
        input_val, expected = value
        actual = power_digit_sum(input_val)
        self.assertEqual(expected, actual)

    @data(("32768", 26), ("32", 5))
    def test_create_sum_from_string(self, value):
        input_val, expected = value
        actual = create_sum_from_string(input_val)
        self.assertEqual(expected, actual)

    @data((32768, "32768"), (32, "32"))
    def test_convert_to_string(self, value):
        input_val, expected = value
        actual = convert_integer_to_string(input_val)
        self.assertEqual(expected, actual)

    @data((15, 32768), (5, 32))
    def test_generate_sum(self, value):
        input_val, expected = value
        actual = generate_exponent_base2(input_val)
        self.assertEqual(expected, actual)

if __name__ == '__main__':
    # unittest.main()
    print(power_digit_sum(1000))
