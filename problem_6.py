"""
Project Euler:  Problem 6
Find the difference between the sum of squares of the first one hundred
natural numbers, and the square of the sum.

The square of the sum of natural numbers is the arithmetic sum squared.

The sum of the squares of natural numbers is a modified arithmetic sum.
"""
import unittest

def sumOfSquares(number):
    return number * (number + 1) * (2 * number + 1) / 6

def squareOfSum(number):
    return pow((number * (number + 1) / 2), 2)

def differenceOfSumsAndSquares(number):
    sum_of_squares = sumOfSquares(number)
    square_of_sum = squareOfSum(number)
    difference = sum_of_squares - square_of_sum
    return difference if difference > 0 else -1 * difference

class TestSquaresAndSums(unittest.TestCase):
    def test_sumOfSquaresTo10_385(self):
        result = sumOfSquares(10)
        self.assertEqual(result, 385)

    def test_squareOfSumTo10_3025(self):
        result = squareOfSum(10)
        self.assertEqual(result, 3025)


if __name__ == '__main__':
    # unittest.main()
    print(differenceOfSumsAndSquares(100))