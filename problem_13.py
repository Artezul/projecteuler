"""
Project Euler Problem 13

Find the first ten numbers of the sum of the following one hundred 50-digit numbers.
"""

data_file = open("problem_13_data.txt", "r")
total = 0
for line in data_file:
    line = line.strip()
    total += int(line)

total = str(total)
print(total[:10])