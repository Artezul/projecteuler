"""
Project Euler Problem 10

Find the summation of all primes below two million.

Identify all numbers that are prime.
Sum up those prime numbers.
Return sum


This function was taken directly from the Problem 7 solution provided after
solving the problem;  it is written by the author: hk.
    1. isPrime(number) - checks if number is prime.
"""
import math
import unittest

def isPrime(number):
    """
    Deliberately made this function to be full of returns for purpose of being
    "optimal", but not extensible.
    """
    # One isn't a prime number
    if number == 1:
        return False

    # Numbers below 4 are 2 and 3, but prime.
    if number < 4:
        return True

    # All even numbers aren't prime.
    if number % 2 == 0:
        return False

    # The number isn't 1, 2, 3, 4, 6, 8 from the previous checks.
    # The remaining numbers are 5, 7.
    if number < 9:
        return True

    # Multiples of 3 aren't prime either.
    if number % 3 == 0:
        return False
    else:
        # Factors above limit DNE, and the number is prime.
        limit = math.floor(math.sqrt(number))

        # Pretty cool
        # With each iteration, we make a leap of 6 numbers, and the resulting
        # numbers are typically prime numbers.
        factor = 5
        while factor <= limit:
            if number % factor == 0:
                return False
            if number % (factor + 2) == 0:
                return False

            factor += 6

    return True

def PrimeSummationUpTo(limit):
    # Includes prime 2 by default
    total = 2
    for num in range(1, limit, 2):
        if isPrime(num):
            total += num

    return total

class TestPrimeSummation(unittest.TestCase):
    def setUp(self):
        self.two_mil = 2000000
        self.answer = 142913828922
    def test_primeSummationSoluationForTwoMillion_142913828922(self):
        """
        Takes a little over 13 seconds to solve.  Not a good unit test,
        but serves as a nice indicator in the future that this is the
        solution to the problem.
        """
        result = PrimeSummationUpTo(self.two_mil)
        self.assertEqual(result, self.answer)

if __name__ == '__main__':
    unittest.main()
